<?php
App::uses('AppController', 'Controller');
/**
 * Books Controller
 *
 * @property Book $Book
 * @property PaginatorComponent $Paginator
 */
class BooksController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('RequestHandler');
    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        return new CakeResponse(array('body' => json_encode($this->Book->find("all"))));
    }

    /**
     * view method
     *
     * @return void
     */
    public function view($id = null)
    {
        return new CakeResponse(array('body' => json_encode($this->Book->find("first",
                array('conditions' => array("Book.id" => $id))))));
    }

    /**
     * view rest
     *
     * @return void
     */
    public function search()
    {
        $parametros = $this->request->params['named'];
        $opcoes = "";

        if (!empty($parametros['id'])) {
            $opcoes['conditions']['Book.id'] = $parametros['id'];
        }
        if (!empty($parametros['isbn'])) {
            $opcoes['conditions']['Book.isbn'] = $parametros['isbn'];
        }

        if (!empty($parametros['title'])) {
            $opcoes['conditions']['OR']['Book.title LIKE'] = "%" . $parametros['title'] .
                "%";
        }

        if (!empty($parametros['author'])) {
            $opcoes['conditions']['OR']['Book.author LIKE'] = "%" . $parametros['author'] .
                "%";
        }

        if (!empty($parametros['publisher'])) {
            $opcoes['conditions']['OR']['Book.publisher LIKE'] = "%" . $parametros['publisher'] .
                "%";
        }

        return new CakeResponse(array('body' => json_encode($this->Book->find("all", $opcoes))));

    }
}
